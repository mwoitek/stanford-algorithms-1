//! Week 3: Programming Assignment
//!
//! Three variants of quicksort

// Type of a function that does partitioning.
type PartitionFn<T> = fn(&mut [T], usize, usize) -> usize;

/// Three possible pivot choices.
pub enum PivotChoice {
    First,
    Last,
    MedianOfThree,
}

// Partitions the array using the first element as the pivot.
fn partition_first<T: Ord>(arr: &mut [T], left: usize, right: usize) -> usize {
    let mut i = left + 1;

    for j in left + 1..=right {
        if arr[j] < arr[left] {
            arr.swap(i, j);
            i += 1;
        }
    }

    let pivot_idx = i - 1;
    arr.swap(pivot_idx, left);
    pivot_idx
}

// Partitions the array using the last element as the pivot.
fn partition_last<T: Ord>(arr: &mut [T], left: usize, right: usize) -> usize {
    arr.swap(left, right);
    partition_first::<T>(arr, left, right)
}

// Computes the median of the first, middle and last elements.
// Returns the index of the median.
fn median_of_three<T: Ord>(arr: &[T], left: usize, right: usize) -> usize {
    let size = right - left + 1;
    let mid_idx = left
        + if size % 2 == 0 {
            size / 2 - 1
        } else {
            (size - 1) / 2
        };

    let first = &arr[left];
    let middle = &arr[mid_idx];
    let last = &arr[right];

    if first >= middle {
        if middle >= last {
            mid_idx
        } else if first >= last {
            right
        } else {
            left
        }
    } else if first >= last {
        left
    } else if middle >= last {
        right
    } else {
        mid_idx
    }
}

// Partitions the array using the median of the first, middle and last elements as the pivot.
fn partition_median_of_three<T: Ord>(arr: &mut [T], left: usize, right: usize) -> usize {
    let median_idx = median_of_three::<T>(arr, left, right);
    arr.swap(left, median_idx);
    partition_first::<T>(arr, left, right)
}

// Recursively sorts the array using the specified partition function.
// Returns the number of comparisons made.
fn quicksort_internal<T: Ord>(
    arr: &mut [T],
    left: usize,
    right: usize,
    partition_fn: PartitionFn<T>,
) -> usize {
    if left >= right {
        return 0;
    }

    let pivot_idx = partition_fn(arr, left, right);
    let mut comparisons = right - left;

    if let Some(diff) = pivot_idx.checked_sub(1) {
        comparisons += quicksort_internal(arr, left, diff, partition_fn);
    }
    comparisons += quicksort_internal(arr, pivot_idx + 1, right, partition_fn);

    comparisons
}

/// Sorts the array using quicksort with the specified pivot choice.
pub fn quicksort<T: Ord>(arr: &mut [T], pivot_choice: PivotChoice) -> usize {
    if arr.is_empty() {
        return 0;
    }
    let partition_fn: PartitionFn<T> = match pivot_choice {
        PivotChoice::First => partition_first,
        PivotChoice::Last => partition_last,
        PivotChoice::MedianOfThree => partition_median_of_three,
    };
    quicksort_internal::<T>(arr, 0, arr.len() - 1, partition_fn)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_partition_first_lecture_example() {
        let mut v: Vec<u8> = vec![3, 8, 2, 5, 1, 4, 7, 6];
        let left = 0;
        let right = 7;

        let idx = partition_first::<u8>(&mut v, left, right);
        let expected_idx = 2;
        assert_eq!(idx, expected_idx);

        let partitioned_v: Vec<u8> = vec![1, 2, 3, 5, 8, 4, 7, 6];
        assert_eq!(v, partitioned_v);
    }

    #[test]
    fn test_partition_first_already_sorted() {
        let mut v: Vec<i32> = vec![1, 2, 3, 4, 5];
        let left = 0;
        let right = 4;

        let idx = partition_first(&mut v, left, right);
        let expected_idx = 0;
        assert_eq!(idx, expected_idx);

        let partitioned_v = vec![1, 2, 3, 4, 5];
        assert_eq!(v, partitioned_v);
    }

    #[test]
    fn test_partition_first_reversed() {
        let mut v: Vec<i32> = vec![5, 4, 3, 2, 1];
        let left = 0;
        let right = 4;

        let idx = partition_first(&mut v, left, right);
        let expected_idx = 4;
        assert_eq!(idx, expected_idx);

        let partitioned_v = vec![1, 4, 3, 2, 5];
        assert_eq!(v, partitioned_v);
    }

    #[test]
    fn test_median_of_three_only_three() {
        let v: Vec<u8> = vec![3, 8, 2];
        let left = 0;
        let right = 2;
        let idx = median_of_three::<u8>(&v, left, right);
        let expected_idx = 0;
        assert_eq!(idx, expected_idx);
    }

    #[test]
    fn test_median_of_three_even_size() {
        let v: Vec<i64> = vec![-2, 3, 10, -1];
        let left = 0;
        let right = 3;
        let idx = median_of_three::<i64>(&v, left, right);
        let expected_idx = 3;
        assert_eq!(idx, expected_idx);
    }

    #[test]
    fn test_median_of_three_all_equal() {
        let v: Vec<i64> = vec![-10; 5];
        let left = 0;
        let right = 4;
        let idx = median_of_three::<i64>(&v, left, right);
        let expected_idx = 2;
        assert_eq!(idx, expected_idx);
    }

    #[test]
    fn test_median_of_three_with_offset_odd_size() {
        let v: Vec<i64> = vec![9, -6, 7, -1, 0];
        let left = 1;
        let right = 3;
        let idx = median_of_three::<i64>(&v, left, right);
        let expected_idx = 3;
        assert_eq!(idx, expected_idx);
    }

    #[test]
    fn test_median_of_three_with_offset_even_size() {
        let v: Vec<i64> = vec![3, -4, 1, 9, -8, 11, -2, 7];
        let left = 2;
        let right = 5;
        let idx = median_of_three::<i64>(&v, left, right);
        let expected_idx = 3;
        assert_eq!(idx, expected_idx);
    }

    #[test]
    fn test_partition_median_of_three_already_sorted() {
        let mut v: Vec<i32> = vec![1, 2, 3, 4, 5];
        let left = 0;
        let right = 4;

        let idx = partition_median_of_three(&mut v, left, right);
        let expected_idx = 2;
        assert_eq!(idx, expected_idx);

        let partitioned_v = vec![1, 2, 3, 4, 5];
        assert_eq!(v, partitioned_v);
    }

    #[test]
    fn test_partition_median_of_three_reversed() {
        let mut v: Vec<i32> = vec![5, 4, 3, 2, 1];
        let left = 0;
        let right = 4;

        let idx = partition_median_of_three(&mut v, left, right);
        let expected_idx = 2;
        assert_eq!(idx, expected_idx);

        let partitioned_v = vec![1, 2, 3, 4, 5];
        assert_eq!(v, partitioned_v);
    }

    #[test]
    fn test_quicksort_partition_first() {
        let mut v: Vec<u8> = vec![3, 8, 2, 5, 1, 4, 7, 6];
        let comparisons = quicksort::<u8>(&mut v, PivotChoice::First);
        let expected_comparisons = 15;
        assert_eq!(comparisons, expected_comparisons);
        let expected_v: Vec<u8> = (1..=8).collect();
        assert_eq!(v, expected_v);
    }

    #[test]
    fn test_quicksort_partition_last() {
        let mut v: Vec<char> = vec!['e', 'b', 'a', 'd', 'c'];
        let comparisons = quicksort::<char>(&mut v, PivotChoice::Last);
        let expected_comparisons = 6;
        assert_eq!(comparisons, expected_comparisons);
        let expected_v: Vec<char> = vec!['a', 'b', 'c', 'd', 'e'];
        assert_eq!(v, expected_v);
    }

    #[test]
    fn test_quicksort_partition_median_of_three() {
        let mut v: Vec<char> = vec!['a', 'b', 'd', 'c'];
        let comparisons = quicksort::<char>(&mut v, PivotChoice::MedianOfThree);
        let expected_comparisons = 4;
        assert_eq!(comparisons, expected_comparisons);
        let expected_v: Vec<char> = vec!['a', 'b', 'c', 'd'];
        assert_eq!(v, expected_v);
    }

    #[test]
    fn test_quicksort_partition_median_of_three_2() {
        let mut v: Vec<u32> = vec![
            2, 20, 1, 15, 3, 11, 13, 6, 16, 10, 19, 5, 4, 9, 8, 14, 18, 17, 7, 12,
        ];
        let comparisons = quicksort::<u32>(&mut v, PivotChoice::MedianOfThree);
        let expected_comparisons = 55;
        assert_eq!(comparisons, expected_comparisons);
        let expected_v: Vec<u32> = (1..=20).collect();
        assert_eq!(v, expected_v);
    }

    #[test]
    fn test_quicksort_empty_vector() {
        let mut v: Vec<i32> = vec![];
        let comparisons = quicksort::<i32>(&mut v, PivotChoice::First);
        let expected_comparisons = 0;
        assert_eq!(comparisons, expected_comparisons);
        let expected_v: Vec<i32> = vec![];
        assert_eq!(v, expected_v);
    }

    #[test]
    fn test_quicksort_with_duplicates() {
        let mut v: Vec<u32> = vec![3, 6, 8, 10, 1, 2, 1];
        quicksort(&mut v, PivotChoice::Last);
        let expected_v: Vec<u32> = vec![1, 1, 2, 3, 6, 8, 10];
        assert_eq!(v, expected_v);
    }
}
