use std::str::FromStr;

use week_1::BigInt;

fn main() {
    println!("Programming Assignments: Answers\n");
    assignment_1();
    println!();
    assignment_2();
    println!();
    assignment_3();
    // println!();
    // assignment_4();
}

fn assignment_1() {
    println!("Assignment 1");
    let num_1 =
        BigInt::from_str("3141592653589793238462643383279502884197169399375105820974944592")
            .unwrap();
    let num_2 =
        BigInt::from_str("2718281828459045235360287471352662497757247093699959574966967627")
            .unwrap();
    let prod = num_1 * num_2;
    println!("Answer: {}", prod); // 8539734222673567065463550869546574495034888535765114961879601127067743044893204848617875072216249073013374895871952806582723184
}

fn assignment_2() {
    println!("Assignment 2");
    let path = "week_2/text_files/IntegerArray.txt";
    if let Ok(mut integers) = week_2::read_integers(path) {
        let inversions = week_2::merge_sort::<i64>(&mut integers);
        println!("Answer: {}", inversions); // 2407905288
    } else {
        println!("Error reading input file!");
    }
}

fn assignment_3() {
    println!("Assignment 3");
    let path = "week_3/text_files/QuickSort.txt";

    let mut int_vectors: Vec<Vec<i64>> = Vec::new();
    if let Ok(int_vector) = week_2::read_integers(path) {
        int_vectors.push(int_vector.clone());
        int_vectors.push(int_vector.clone());
        int_vectors.push(int_vector);
    } else {
        println!("Error reading input file!");
    }

    let comparisons_1 = week_3::quicksort::<i64>(&mut int_vectors[0], week_3::PivotChoice::First);
    println!("Answer 1: {}", comparisons_1); // 162085

    let comparisons_2 = week_3::quicksort::<i64>(&mut int_vectors[1], week_3::PivotChoice::Last);
    println!("Answer 2: {}", comparisons_2); // 164123

    let comparisons_3 =
        week_3::quicksort::<i64>(&mut int_vectors[2], week_3::PivotChoice::MedianOfThree);
    println!("Answer 3: {}", comparisons_3); // 138382
}
