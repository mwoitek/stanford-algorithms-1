//! Week 2: Programming Assignment
//!
//! Merge sort and number of inversions

use std::cmp::Ordering;
use std::fs::File;
use std::io::{self, BufRead, BufReader};
use std::mem;
use std::num::ParseIntError;
use std::path::Path;

/// Custom error type to handle different kinds of errors
/// when reading a text file with integers.
#[derive(Debug)]
pub enum ReadError {
    Io(io::Error),
    Parse(ParseIntError),
}

impl From<io::Error> for ReadError {
    fn from(err: io::Error) -> ReadError { ReadError::Io(err) }
}

impl From<ParseIntError> for ReadError {
    fn from(err: ParseIntError) -> ReadError { ReadError::Parse(err) }
}

/// Function to read integers from a file.
pub fn read_integers<P: AsRef<Path>>(path: P) -> Result<Vec<i64>, ReadError> {
    let file = File::open(path)?;
    let reader = BufReader::new(file);

    let mut integers: Vec<i64> = Vec::new();

    for line in reader.lines() {
        let line = line?; // line on the RHS is Result<String, Error>
        let number: i64 = line.trim().parse()?;
        integers.push(number);
    }

    Ok(integers)
}

/// Helper function to compute the midpoint between two indexes.
fn midpoint(low: usize, high: usize) -> usize {
    let diff = high as f64 - low as f64;
    let mid = (low as f64 + diff / 2.0).floor();
    mid as usize
}

/// Helper function that implements the merge step of the merge sort algorithm.
fn merge<T>(v: &mut Vec<T>, low: usize, mid: usize, high: usize) -> usize
where
    T: Ord + Clone + Default,
{
    let mut inversions = 0;

    let mut merged: Vec<T> = Vec::with_capacity(high + 1 - low);
    let (mut i, mut j) = (low, mid + 1);
    while i <= mid && j <= high {
        match v[i].cmp(&v[j]) {
            Ordering::Less | Ordering::Equal => {
                merged.push(mem::take(&mut v[i]));
                i += 1;
            }
            Ordering::Greater => {
                merged.push(mem::take(&mut v[j]));
                j += 1;
                inversions += mid + 1 - i;
            }
        }
    }
    merged.extend_from_slice(if i > mid { &v[j..=high] } else { &v[i..=mid] });

    v.splice(low..=high, merged);
    inversions
}

/// Merge sort implementation. This version is kept private.
fn merge_sort_internal<T>(v: &mut Vec<T>, low: usize, high: usize) -> usize
where
    T: Ord + Clone + Default,
{
    let mut inversions = 0;

    if low < high {
        let mid = midpoint(low, high);
        inversions += merge_sort_internal(v, low, mid);
        inversions += merge_sort_internal(v, mid + 1, high);
        inversions += merge(v, low, mid, high);
    }

    inversions
}

/// Merge sort implementation.
pub fn merge_sort<T>(v: &mut Vec<T>) -> usize
where
    T: Ord + Clone + Default,
{
    if v.is_empty() {
        return 0;
    }
    merge_sort_internal(v, 0, v.len() - 1)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_read_integers_all_non_negative() {
        let path = "text_files/test_1.txt";
        let result = read_integers(path);
        assert!(result.is_ok());
        let integers: Vec<i64> = vec![0, 1, 2, 3, 4];
        assert_eq!(result.unwrap(), integers);
    }

    #[test]
    fn test_read_integers_has_negative_integers() {
        let path = "text_files/test_2.txt";
        let result = read_integers(path);
        assert!(result.is_ok());
        let integers: Vec<i64> = vec![-4, -3, -2, -1, 0, 1, 2, 3, 4];
        assert_eq!(result.unwrap(), integers);
    }

    #[test]
    fn test_read_integers_non_existing_file() {
        let path = "text_files/non_existing.txt";
        let result = read_integers(path);
        assert!(result.is_err());
        assert!(matches!(result.unwrap_err(), ReadError::Io(_)));
    }

    #[test]
    fn test_read_integers_has_non_integer_line() {
        let path = "text_files/test_3.txt";
        let result = read_integers(path);
        assert!(result.is_err());
        assert!(matches!(result.unwrap_err(), ReadError::Parse(_)));
    }

    #[test]
    fn test_merge_no_inversion() {
        let mut v: Vec<u32> = vec![1, 2, 3, 4, 5, 6];
        let low = 0;
        let mid = 2;
        let high = 5;
        let inversions = merge::<u32>(&mut v, low, mid, high);
        let expected_inversions = 0;
        assert_eq!(inversions, expected_inversions);
    }

    #[test]
    fn test_merge_single_inversion() {
        let mut v: Vec<i32> = vec![-3, -2, 0, -1, 2, 3];
        let low = 0;
        let mid = 2;
        let high = 5;
        let inversions = merge::<i32>(&mut v, low, mid, high);
        let expected_inversions = 1;
        assert_eq!(inversions, expected_inversions);
    }

    #[test]
    fn test_merge_left_with_larger_entries() {
        let mut v: Vec<i32> = vec![1, 2, 3, -2, -1, 0];
        let low = 0;
        let mid = 2;
        let high = 5;
        let inversions = merge::<i32>(&mut v, low, mid, high);
        let expected_inversions = 9;
        assert_eq!(inversions, expected_inversions);
    }

    #[test]
    fn test_merge_sort() {
        let mut v: Vec<u32> = vec![5, 3, 8, 4, 2, 7, 6, 1];
        let inversions = merge_sort::<u32>(&mut v);
        let expected_v: Vec<u32> = vec![1, 2, 3, 4, 5, 6, 7, 8];
        assert_eq!(v, expected_v);
        let expected_inversions = 17;
        assert_eq!(inversions, expected_inversions);
    }

    #[test]
    fn test_merge_sort_with_duplicates() {
        let mut v: Vec<u32> = vec![3, 3, 2, 1, 1, 2];
        let inversions = merge_sort::<u32>(&mut v);
        let expected_v: Vec<u32> = vec![1, 1, 2, 2, 3, 3];
        assert_eq!(v, expected_v);
        let expected_inversions = 10;
        assert_eq!(inversions, expected_inversions);
    }

    #[test]
    fn test_merge_sort_empty() {
        let mut v: Vec<u32> = vec![];
        let inversions = merge_sort::<u32>(&mut v);
        let expected_v: Vec<u32> = vec![];
        assert_eq!(v, expected_v);
        let expected_inversions = 0;
        assert_eq!(inversions, expected_inversions);
    }

    #[test]
    fn test_merge_sort_single_element() {
        let mut v: Vec<u32> = vec![1];
        let inversions = merge_sort::<u32>(&mut v);
        let expected_v: Vec<u32> = vec![1];
        assert_eq!(v, expected_v);
        let expected_inversions = 0;
        assert_eq!(inversions, expected_inversions);
    }

    #[test]
    fn test_merge_sort_char_vector() {
        let mut v: Vec<char> = vec!['a', 'd', 'c', 'b'];
        let inversions = merge_sort::<char>(&mut v);
        let expected_v: Vec<char> = vec!['a', 'b', 'c', 'd'];
        assert_eq!(v, expected_v);
        let expected_inversions = 3;
        assert_eq!(inversions, expected_inversions);
    }

    #[test]
    fn test_merge_sort_string_vector() {
        let mut v: Vec<String> = vec![
            "Emma".to_string(),
            "Charlie".to_string(),
            "Alice".to_string(),
            "Daniel".to_string(),
            "Bob".to_string(),
        ];
        let inversions = merge_sort::<String>(&mut v);
        let expected_v: Vec<String> = vec![
            "Alice".to_string(),
            "Bob".to_string(),
            "Charlie".to_string(),
            "Daniel".to_string(),
            "Emma".to_string(),
        ];
        assert_eq!(v, expected_v);
        let expected_inversions = 7;
        assert_eq!(inversions, expected_inversions);
    }

    #[test]
    fn test_merge_sort_sorted() {
        let mut v: Vec<i32> = (-10..10).collect();
        let inversions = merge_sort::<i32>(&mut v);
        let expected_v: Vec<i32> = (-10..10).collect();
        assert_eq!(v, expected_v);
        let expected_inversions = 0;
        assert_eq!(inversions, expected_inversions);
    }

    #[test]
    fn test_merge_sort_reverse_sorted() {
        let mut v: Vec<u32> = (10..20).rev().collect();
        let inversions = merge_sort::<u32>(&mut v);
        let expected_v: Vec<u32> = (10..20).collect();
        assert_eq!(v, expected_v);
        let expected_inversions = 45;
        assert_eq!(inversions, expected_inversions);
    }

    #[test]
    fn test_merge_sort_large() {
        let mut v: Vec<u32> = (0..10000).rev().collect();
        let inversions = merge_sort::<u32>(&mut v);
        let expected_v: Vec<u32> = (0..10000).collect();
        assert_eq!(v, expected_v);
        let expected_inversions = 49995000;
        assert_eq!(inversions, expected_inversions);
    }
}
