//! Week 1: Programming Assignment
//!
//! Karatsuba multiplication

use std::cmp::Ordering;
use std::fmt;
use std::ops::{Add, Mul, Neg, Sub};
use std::str::FromStr;

/// Naive implementation of an arbitrary-precision integer. An integer is
/// represented by a vector of digits. The least significative digit is in the
/// first position.
#[derive(Debug, Clone, PartialEq)]
pub struct BigInt {
    digits: Vec<u8>,
    sign: bool, // true for negative
}

impl BigInt {
    /// Helper function to remove leading zeros from a digits vector.
    fn remove_leading_zeros(a: &mut Vec<u8>) {
        while a.len() > 1 && a.last() == Some(&0) {
            a.pop();
        }
    }

    /// Helper function to check if a BigInt equals zero.
    fn equals_zero(&self) -> bool { self.digits.len() == 1 && self.digits[0] == 0 }

    /// Construct a normalized BigInt.
    pub fn new(digits: Vec<u8>, sign: bool) -> Self {
        let mut bigint = Self { digits, sign };
        Self::remove_leading_zeros(&mut bigint.digits);
        if bigint.equals_zero() {
            bigint.sign = false;
        }
        bigint
    }

    /// Helper function to compare digit vectors.
    /// This function returns how the vector a relates to b.
    /// This is used to implement subtraction.
    fn compare_digits(a: &[u8], b: &[u8]) -> Ordering {
        if a.len() != b.len() {
            return a.len().cmp(&b.len());
        }

        let iter_a = a.iter().rev();
        let iter_b = b.iter().rev();

        for (digit_a, digit_b) in iter_a.zip(iter_b) {
            if digit_a != digit_b {
                return digit_a.cmp(digit_b);
            }
        }

        Ordering::Equal
    }

    /// Helper function to add two digit vectors.
    fn add_digits(a: &[u8], b: &[u8]) -> Vec<u8> {
        let len_a = a.len();
        let len_b = b.len();
        let max_len = len_a.max(len_b);

        let mut result: Vec<u8> = Vec::with_capacity(max_len + 1);
        let mut carry: u8 = 0;

        for i in 0..max_len {
            let digit_a = if i < len_a { a[i] } else { 0 };
            let digit_b = if i < len_b { b[i] } else { 0 };
            let sum = digit_a + digit_b + carry;
            result.push(sum % 10);
            carry = sum / 10;
        }

        if carry > 0 {
            result.push(carry);
        }

        result
    }

    /// Helper function to subtract two digit vectors.
    /// Computes a - b, where a >= b.
    fn sub_digits(a: &[u8], b: &[u8]) -> Vec<u8> {
        let len_a = a.len();
        let len_b = b.len();

        let mut result: Vec<u8> = Vec::with_capacity(len_a);
        let mut borrow: i16 = 0;

        for i in 0..len_a {
            let digit_a = a[i] as i16;
            let digit_b = if i < len_b { b[i] as i16 } else { 0 };
            let mut diff = digit_a - digit_b - borrow;
            if diff < 0 {
                diff += 10;
                borrow = 1;
            } else {
                borrow = 0;
            }
            result.push(diff as u8);
        }

        Self::remove_leading_zeros(&mut result);
        result
    }

    /// Helper function to multiply a digits vector by a 1-digit number.
    fn multiply_by_digit(a: &[u8], d: u8) -> Vec<u8> {
        if d == 0 {
            return vec![0];
        }
        if d == 1 {
            return Vec::from(a);
        }

        let mut result: Vec<u8> = Vec::with_capacity(a.len() + 1);
        let mut carry: u8 = 0;

        for &digit in a {
            let prod = digit * d + carry;
            result.push(prod % 10);
            carry = prod / 10;
        }

        if carry > 0 {
            result.push(carry);
        }

        result
    }

    /// Helper function to add zeros to the front of a digits vector.
    fn left_shift(a: &[u8], m: usize) -> Vec<u8> {
        if m == 0 {
            return Vec::from(a);
        }
        let mut result: Vec<u8> = vec![0; m];
        result.extend_from_slice(a);
        result
    }

    /// Helper function to multiply 2 digit vectors using the Karatsuba algorithm.
    fn karatsuba(a: &[u8], b: &[u8]) -> Vec<u8> {
        if a.len() == 1 {
            return Self::multiply_by_digit(b, a[0]);
        }
        if b.len() == 1 {
            return Self::multiply_by_digit(a, b[0]);
        }

        let max_len = a.len().max(b.len());
        // NOTE: Using div_ceil here can produce empty digit vectors. In such
        // cases, we'll have a panic when split_at is called.
        // let m = max_len.div_ceil(2);
        let m = max_len / 2;

        let (low_a, high_a) = a.split_at(m);
        let (low_b, high_b) = b.split_at(m);

        let z0 = Self::karatsuba(low_a, low_b);
        let z2 = Self::karatsuba(high_a, high_b);
        let z3 = Self::karatsuba(
            &Self::add_digits(low_a, high_a),
            &Self::add_digits(low_b, high_b),
        );

        let mut z1 = Self::sub_digits(&z3, &z2);
        z1 = Self::sub_digits(&z1, &z0);

        let mut result = Self::add_digits(&Self::left_shift(&z1, m), &Self::left_shift(&z2, 2 * m));
        result = Self::add_digits(&result, &z0);

        result
    }
}

impl FromStr for BigInt {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let trimmed = s.trim();
        if trimmed.is_empty() {
            return Err("Input is empty".to_string());
        }

        let (sign, digits_str) = if let Some(stripped) = trimmed.strip_prefix('-') {
            (true, stripped)
        } else {
            (false, trimmed)
        };

        if sign && digits_str.is_empty() {
            return Err("No digits found".to_string());
        }

        let mut digits: Vec<u8> = Vec::with_capacity(digits_str.len());
        for c in digits_str.chars().rev() {
            if let Some(digit) = c.to_digit(10) {
                digits.push(digit as u8);
            } else {
                return Err(format!("Non-digit character found: {}", c));
            }
        }

        Ok(Self::new(digits, sign))
    }
}

impl fmt::Display for BigInt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s = self
            .digits
            .iter()
            .rev()
            .map(|&d| (d + 48) as char)
            .collect::<String>();
        if self.sign {
            s.insert(0, '-');
        }
        write!(f, "{}", s)
    }
}

impl Add for BigInt {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        if self.sign == other.sign {
            // Same signs: Add the absolute values
            return Self {
                digits: Self::add_digits(&self.digits, &other.digits),
                sign: self.sign,
            };
        }
        // Different signs: Subtract the smaller absolute value from the larger
        // absolute value
        match Self::compare_digits(&self.digits, &other.digits) {
            Ordering::Greater => Self {
                digits: Self::sub_digits(&self.digits, &other.digits),
                sign: self.sign,
            },
            Ordering::Less => Self {
                digits: Self::sub_digits(&other.digits, &self.digits),
                sign: other.sign,
            },
            Ordering::Equal => Self { digits: vec![0], sign: false },
        }
    }
}

impl Neg for BigInt {
    type Output = Self;

    fn neg(self) -> Self::Output {
        let sign = if self.equals_zero() {
            false
        } else {
            !self.sign
        };
        Self { digits: self.digits, sign }
    }
}

impl Sub for BigInt {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output { self + (-other) }
}

impl Mul for BigInt {
    type Output = Self;

    #[allow(clippy::suspicious_arithmetic_impl)]
    fn mul(self, other: Self) -> Self::Output {
        let digits = Self::karatsuba(&self.digits, &other.digits);
        let sign = self.sign ^ other.sign;
        Self { digits, sign }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_valid_positive_input() {
        let input = "12345";
        let expected = BigInt { digits: vec![5, 4, 3, 2, 1], sign: false };
        assert_eq!(input.parse::<BigInt>(), Ok(expected));
    }

    #[test]
    fn test_valid_zero_input() {
        let input = "-00000";
        let expected = BigInt { digits: vec![0], sign: false };
        assert_eq!(input.parse::<BigInt>(), Ok(expected));
    }

    #[test]
    fn test_valid_negative_input() {
        let input = "-67890";
        let expected = BigInt { digits: vec![0, 9, 8, 7, 6], sign: true };
        assert_eq!(input.parse::<BigInt>(), Ok(expected));
    }

    #[test]
    fn test_valid_input_with_leading_zeros() {
        let input = "000120";
        let expected = BigInt { digits: vec![0, 2, 1], sign: false };
        assert_eq!(input.parse::<BigInt>(), Ok(expected));
    }

    #[test]
    fn test_valid_input_with_extra_whitespace() {
        let input = "  93210 ";
        let expected = BigInt { digits: vec![0, 1, 2, 3, 9], sign: false };
        assert_eq!(input.parse::<BigInt>(), Ok(expected));
    }

    #[test]
    fn test_invalid_empty_input() {
        let input = "          ";
        let expected_msg = "Input is empty".to_string();
        assert_eq!(input.parse::<BigInt>(), Err(expected_msg));
    }

    #[test]
    fn test_invalid_input_with_only_negative_sign() {
        let input = " -   ";
        let expected_msg = "No digits found".to_string();
        assert_eq!(input.parse::<BigInt>(), Err(expected_msg));
    }

    #[test]
    fn test_invalid_input_with_non_digit_character() {
        let input = "12a34";
        let expected_msg = "Non-digit character found: a".to_string();
        assert_eq!(input.parse::<BigInt>(), Err(expected_msg));
    }

    #[test]
    fn test_add_same_sign_positive() {
        let a = BigInt::from_str("123").unwrap();
        let b = BigInt::from_str("456").unwrap();
        let result = a + b;
        assert_eq!(result, BigInt::from_str("579").unwrap());
    }

    #[test]
    fn test_add_same_sign_negative() {
        let a = BigInt::from_str("-123").unwrap();
        let b = BigInt::from_str("-456").unwrap();
        let result = a + b;
        assert_eq!(result, BigInt::from_str("-579").unwrap());
    }

    #[test]
    fn test_add_different_signs_positive_greater() {
        let a = BigInt::from_str("465").unwrap();
        let b = BigInt::from_str("-132").unwrap();
        let result = a + b;
        assert_eq!(result, BigInt::from_str("333").unwrap());
    }

    #[test]
    fn test_add_different_signs_negative_greater() {
        let a = BigInt::from_str("132").unwrap();
        let b = BigInt::from_str("-465").unwrap();
        let result = a + b;
        assert_eq!(result, BigInt::from_str("-333").unwrap());
    }

    #[test]
    fn test_add_positive_and_negative_equal() {
        let a = BigInt::from_str("345").unwrap();
        let b = BigInt::from_str("-345").unwrap();
        let result = a + b;
        assert_eq!(result, BigInt::from_str("0").unwrap());
    }

    #[test]
    fn test_add_integer_to_zero() {
        let a = BigInt::from_str("0").unwrap();
        let b = BigInt::from_str("345").unwrap();
        let result = a + b;
        assert_eq!(result, BigInt::from_str("345").unwrap());
    }

    #[test]
    fn test_add_zero_to_integer() {
        let a = BigInt::from_str("345").unwrap();
        let b = BigInt::from_str("0").unwrap();
        let result = a + b;
        assert_eq!(result, BigInt::from_str("345").unwrap());
    }

    #[test]
    fn test_addition_is_commutative() {
        let a = BigInt::from_str("345").unwrap();
        let b = BigInt::from_str("132").unwrap();
        let result_1 = a.clone() + b.clone();
        let result_2 = b + a;
        assert_eq!(result_1, result_2);
        assert_eq!(result_1, BigInt::from_str("477").unwrap());
    }

    #[test]
    fn test_add_small_to_large_integer() {
        let a = BigInt::from_str("9876543210").unwrap();
        let b = BigInt::from_str("5").unwrap();
        let result = a + b;
        assert_eq!(result, BigInt::from_str("9876543215").unwrap());
    }

    #[test]
    fn test_add_two_large_integers() {
        let a = BigInt::from_str("123456789").unwrap();
        let b = BigInt::from_str("2345678901").unwrap();
        let result = a + b;
        assert_eq!(result, BigInt::from_str("2469135690").unwrap());
    }

    #[test]
    fn test_neg_positive() {
        let a = BigInt::from_str("345").unwrap();
        let result = -a;
        assert_eq!(result, BigInt::from_str("-345").unwrap());
    }

    #[test]
    fn test_neg_zero() {
        let a = BigInt::from_str("0").unwrap();
        let result = -a.clone();
        assert_eq!(result, a);
    }

    #[test]
    fn test_neg_negative() {
        let a = BigInt::from_str("-2469").unwrap();
        let result = -a;
        assert_eq!(result, BigInt::from_str("2469").unwrap());
    }

    #[test]
    fn test_sub_same_sign_positive() {
        let a = BigInt::from_str("456").unwrap();
        let b = BigInt::from_str("123").unwrap();
        let result = a - b;
        assert_eq!(result, BigInt::from_str("333").unwrap());
    }

    #[test]
    fn test_sub_same_sign_negative() {
        let a = BigInt::from_str("-541").unwrap();
        let b = BigInt::from_str("-852").unwrap();
        let result = a - b;
        assert_eq!(result, BigInt::from_str("311").unwrap());
    }

    #[test]
    fn test_sub_different_signs_positive_difference() {
        let a = BigInt::from_str("345").unwrap();
        let b = BigInt::from_str("-132").unwrap();
        let result = a - b;
        assert_eq!(result, BigInt::from_str("477").unwrap());
    }

    #[test]
    fn test_sub_different_signs_negative_difference() {
        let a = BigInt::from_str("365").unwrap();
        let b = BigInt::from_str("874").unwrap();
        let result = a - b;
        assert_eq!(result, BigInt::from_str("-509").unwrap());
    }

    #[test]
    fn test_sub_to_zero() {
        let a = BigInt::from_str("1356").unwrap();
        let b = BigInt::from_str("1356").unwrap();
        let result = a - b;
        assert_eq!(result, BigInt::from_str("0").unwrap());
    }

    #[test]
    fn test_sub_zero() {
        let a = BigInt::from_str("789").unwrap();
        let b = BigInt::from_str("0").unwrap();
        let result = a - b;
        assert_eq!(result, BigInt::from_str("789").unwrap());
    }

    #[test]
    fn test_sub_change_order() {
        let a = BigInt::from_str("628").unwrap();
        let b = BigInt::from_str("-937").unwrap();
        let result_1 = a.clone() - b.clone();
        let result_2 = b - a;
        assert_eq!(result_1, -result_2);
        assert_eq!(result_1, BigInt::from_str("1565").unwrap());
    }

    #[test]
    fn test_sub_small_from_large() {
        let a = BigInt::from_str("2469135690").unwrap();
        let b = BigInt::from_str("2").unwrap();
        let result = a - b;
        assert_eq!(result, BigInt::from_str("2469135688").unwrap());
    }

    #[test]
    fn test_sub_large_from_small() {
        let a = BigInt::from_str("7").unwrap();
        let b = BigInt::from_str("2345678901").unwrap();
        let result = a - b;
        assert_eq!(result, BigInt::from_str("-2345678894").unwrap());
    }

    #[test]
    fn test_sub_two_large_integers() {
        let a = BigInt::from_str("23456789012345678901").unwrap();
        let b = BigInt::from_str("123456789123456789").unwrap();
        let result = a - b;
        assert_eq!(result, BigInt::from_str("23333332223222222112").unwrap());
    }

    #[test]
    fn test_multiply_by_digit_zero() {
        let a: &[u8] = &[5, 4, 3]; // 345
        let d = 0;
        let result = BigInt::multiply_by_digit(a, d);
        assert_eq!(result, vec![0]);
    }

    #[test]
    fn test_multiply_by_digit_one() {
        let a: &[u8] = &[5, 4, 3]; // 345
        let d = 1;
        let result = BigInt::multiply_by_digit(a, d);
        assert_eq!(result, a);
    }

    #[test]
    fn test_multiply_by_digit() {
        let a: &[u8] = &[2, 4, 3]; // 342
        let d = 3;
        let result = BigInt::multiply_by_digit(a, d);
        assert_eq!(result, vec![6, 2, 0, 1]); // 1026
    }

    #[test]
    fn test_multiply_by_digit_with_carry() {
        let a: &[u8] = &[9, 9, 9]; // 999
        let d = 9;
        let result = BigInt::multiply_by_digit(a, d);
        assert_eq!(result, vec![1, 9, 9, 8]); // 8991
    }

    #[test]
    fn test_left_shift() {
        let a: &[u8] = &[1, 2, 3]; // 321
        let m = 2;
        let result = BigInt::left_shift(a, m);
        assert_eq!(result, vec![0, 0, 1, 2, 3]); // 32100
    }

    #[test]
    fn test_left_shift_no_padding() {
        let a: &[u8] = &[6, 7, 8, 9]; // 9876
        let m = 0;
        let result = BigInt::left_shift(a, m);
        assert_eq!(result, vec![6, 7, 8, 9]); // 9876
    }

    #[test]
    fn test_mul_same_sign_positive() {
        let a = BigInt::from_str("123").unwrap();
        let b = BigInt::from_str("456").unwrap();
        let result = a * b;
        assert_eq!(result, BigInt::from_str("56088").unwrap());
    }

    #[test]
    fn test_mul_same_sign_negative() {
        let a = BigInt::from_str("-7651").unwrap();
        let b = BigInt::from_str("-9237").unwrap();
        let result = a * b;
        assert_eq!(result, BigInt::from_str("70672287").unwrap());
    }

    #[test]
    fn test_mul_different_signs() {
        let a = BigInt::from_str("5524").unwrap();
        let b = BigInt::from_str("-8604").unwrap();
        let result = a * b;
        assert_eq!(result, BigInt::from_str("-47528496").unwrap());
    }

    #[test]
    fn test_mul_by_zero() {
        let a = BigInt::from_str("3514").unwrap();
        let b = BigInt::from_str("0").unwrap();
        let result = a * b;
        assert_eq!(result, BigInt::from_str("0").unwrap());
    }

    #[test]
    fn test_mul_by_one() {
        let a = BigInt::from_str("-214").unwrap();
        let b = BigInt::from_str("1").unwrap();
        let result = a.clone() * b;
        assert_eq!(result, a);
    }

    #[test]
    fn test_mul_by_minus_one() {
        let a = BigInt::from_str("-214").unwrap();
        let b = BigInt::from_str("-1").unwrap();
        let result = a.clone() * b;
        assert_eq!(result, -a);
    }

    #[test]
    fn test_multiplication_is_commutative() {
        let a = BigInt::from_str("741").unwrap();
        let b = BigInt::from_str("963").unwrap();
        let result_1 = a.clone() * b.clone();
        let result_2 = b * a;
        assert_eq!(result_1, result_2);
        assert_eq!(result_1, BigInt::from_str("713583").unwrap());
    }

    #[test]
    fn test_mul_small_and_large() {
        let a = BigInt::from_str("4").unwrap();
        let b = BigInt::from_str("987654321987654321").unwrap();
        let result = a * b;
        assert_eq!(result, BigInt::from_str("3950617287950617284").unwrap());
    }

    #[test]
    fn test_mul_large_and_small() {
        let a = BigInt::from_str("23333332223222222112").unwrap();
        let b = BigInt::from_str("9").unwrap();
        let result = a * b;
        assert_eq!(result, BigInt::from_str("209999990008999999008").unwrap());
    }

    #[test]
    fn test_mul_two_large_integers() {
        let a = BigInt::from_str("3950617287950617284").unwrap();
        let b = BigInt::from_str("209999990008999999008").unwrap();
        let result = a * b;
        assert_eq!(
            result,
            BigInt::from_str("829629590999012301806370365908987654272").unwrap()
        );
    }
}
